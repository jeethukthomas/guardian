//
//  ExtViewController.swift
//  MLKit
//
//  Created by Jeethu Thomas on 25/05/18.
//  Copyright © 2018 Jeethu Thomas. All rights reserved.
//

import UIKit
import Firebase

extension ViewController {
    
    /// Loads the local model.
    func loadLocalModel() {
        guard let localModelFilePath = Bundle.main.path(
            forResource: Constants.quantizedModelFilename,
            ofType: DetectorConstants.modelExtension
            ),
            let labelsFilePath = Bundle.main.path(
                forResource: Constants.quantizedLabelsFilename,
                ofType: DetectorConstants.labelsExtension
            )
            else {
                resultsTextView.text = "Failed to get the paths to the local model and labels files."
                return
        }
        let localModelSource = LocalModelSource(
            modelName: Constants.localModelName,
            path: localModelFilePath
        )
        let modelManager = ModelManager.modelManager()
        
        if !modelManager.register(localModelSource) {
            print("Model source was already registered with name: \(localModelSource.modelName).")
        }
        let options = ModelOptions(cloudModelName: nil, localModelName: Constants.localModelName)
        detectorService.loadModel(options: options, labelsPath: labelsFilePath)
    }
    
    func detectObjects(withImage image: UIImage) {
            loadLocalModel()
    
        DispatchQueue.global(qos: .userInitiated).async {
            let imageData = self.detectorService.scaledImageData(for: image)
            self.detectorService.detectObjects(imageData: imageData) { (results, error) in
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.3, execute: {
                    if self.frameExtractor != nil {
                        self.frameExtractor.wait = false
                    }
                })
                guard error == nil, let results = results, !results.isEmpty else {
                    
                    
                    let errorString = error?.localizedDescription ?? Constants.failedToDetectObjectsMessage
                    self.resultsTextView.text = "Inference error: \(errorString)"
                    return
                }
                
                let inferenceMessageString = ""
                DispatchQueue.main.async {
                    self.resultsTextView.text =
                        inferenceMessageString + "\(self.detectionResultsString(fromResults: results))"
                }
                
                for tResult in results {
//                    if tResult.confidence >= labelConfidenceThreshold {
                    if enemies.contains(tResult.label.lowercased()) {
                        self.enemiesDetected()
                        
                    }
//                    }
                }
                
            }
        }
    }

    private func detectionResultsString(
        fromResults results: [(label: String, confidence: Float)]?
        ) -> String {
        guard let results = results else { return Constants.failedToDetectObjectsMessage }
        return results.reduce("") { (resultString, result) -> String in
            let (label, confidence) = result
//            if confidence >= labelConfidenceThreshold {
            return resultString + "\(label)\n"
//            }
//            else {
//                return ""
//            }
        }
    }
}



// MARK: - Fileprivate

fileprivate enum Constants {
    // TODO: REPLACE THESE CLOUD MODEL NAMES WITH ONES THAT ARE UPLOADED TO YOUR FIREBASE CONSOLE.
    static let cloudModelName1 = "image_classification"
    static let cloudModelName2 = "invalid_model"
    
    static let localModelName = "mobilenet"
    
    static let multiFaceImage = "multi-face.png"
    static let graceHopperImage = "grace_hopper.jpg"
    
    static let labelsFilename = "labels"
    static let modelFilename = "mobilenet_v1_1.0_224"
    static let quantizedLabelsFilename = "labels_quant"
    static let quantizedModelFilename = "mobilenet_quant_v1_224"
    
    static let cloudModel1DownloadCompletedKey = "FIRCloudModel1DownloadCompleted"
    static let cloudModel2DownloadCompletedKey = "FIRCloudModel2DownloadCompleted"
    
    static let detectionNoResultsMessage = "No results returned."
    static let failedToDetectObjectsMessage = "Failed to detect objects in image."
    
    static let labelConfidenceThreshold: Float = 0.75
    static let lineWidth: CGFloat = 3.0
    static let lineColor = UIColor.yellow.cgColor
    static let fillColor = UIColor.clear.cgColor
}
