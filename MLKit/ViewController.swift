//
//  ViewController.swift
//  MLKit
//
//  Created by Jeethu Thomas on 23/05/18.
//  Copyright © 2018 Jeethu Thomas. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation
let labelConfidenceThreshold: Float = 0.5
let detectionNoResultsMessage = "No results returned."


var enemies = [String]()
class ViewController: UIViewController, FrameExtractorDelegate {
   
    @IBOutlet weak var videoV: UIView!
    @IBOutlet weak var resultsTextView: UITextView!
    lazy var vision = Vision.vision()
    
    var detectView : UIView!
    
    var frameExtractor : FrameExtractor!
     let detectorService = DetectorService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let val = UserDefaults.standard.object(forKey: "Enemies") as? [String] {
            enemies = val
        }
        else {
            UserDefaults.standard.set(["nothing"], forKey: "Enemies")
            enemies = ["nothing"]
        }
        
       self.detectView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.detectView.backgroundColor = UIColor.clear
        self.detectView.layer.borderWidth = 3.0
        self.detectView.layer.borderColor = UIColor.red.cgColor
        self.view.addSubview(self.detectView)
        
    }

    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.5) {
            self.frameExtractor = FrameExtractor(previewView: self.videoV)
            self.frameExtractor.delegate = self
        }
        
        if !UserDefaults.standard.bool(forKey: "AboutApp") {
            UserDefaults.standard.set(true, forKey: "AboutApp")
            let alert = UIAlertController(title: "About the App", message: "The use case of this app is to keep the baby safe in a room even when the parents are around. You can add objects like animals, birds etc and get alerted when ever it comes near by your baby.", preferredStyle: UIAlertControllerStyle.alert)
            let cancelBtn = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.destructive) { (action) in
                
            }
            alert.addAction(cancelBtn)
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        
        self.frameExtractor.delegate = nil
        self.frameExtractor = nil
    }
    func captured(image: UIImage) {
//        self.detectObjects(withImage: image)
        self.detectLabels(withImage: image)
//        self.detectLabelsCloud(withImage: image)
    }
    
  
    
    func detectLabels(withImage image: UIImage) {
        
        // [START config_label]
        let options = VisionLabelDetectorOptions(
            confidenceThreshold: labelConfidenceThreshold
        )
        // [END config_label]
        
        // [START init_label]
        let labelDetector = vision.labelDetector(options: options)  // Check console for errors.
        // Or, to use the default settings:
        // let labelDetector = vision?.labelDetector()
        // [END init_label]
        
        // Define the metadata for the image.
        let imageMetadata = VisionImageMetadata()
        imageMetadata.orientation = detectorOrientationFrom(image.imageOrientation)
        
        // Initialize a VisionImage object with the given UIImage.
        let visionImage = VisionImage(image: image)
        visionImage.metadata = imageMetadata
        
        // [START detect_label]
        labelDetector.detect(in: visionImage) { (labels, error) in
            
        
            
         
            guard error == nil, let tLabels = labels, !tLabels.isEmpty else {
                // Error. You should also check the console for error messages.
                // [START_EXCLUDE]
                let errorString = detectionNoResultsMessage
//                print("Label detection failed with error: \(errorString)")
                DispatchQueue.main.async {
                    self.resultsTextView.text = "No Objects!"
                }
                
                // [END_EXCLUDE]
                
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.3, execute: {
                    if self.frameExtractor != nil {
                        self.frameExtractor.wait = false
                    }
                })
                return
            }
            
//            print("Labels :  \(tLabels)")
            // Labeled image
            // [START_EXCLUDE]
            
            DispatchQueue.main.async {
            self.resultsTextView.text = tLabels.map { label -> String in
                
                
                
                
                    "\(label.label)"
                    }.joined(separator: "\n")
            }
            
            
            
            for label in tLabels {
                if enemies.contains(label.label.lowercased()) {
                    self.enemiesDetected()
                    DispatchQueue.main.async {
                    self.detectView.frame = label.frame
                    }
                }
            }
            
            
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.3, execute: {
                    if self.frameExtractor != nil {
                        self.frameExtractor.wait = false
                    }
                })
            
            // [END_EXCLUDE]
        }
        // [END detect_label]
    }

    /// Detects labels on the specified image and prints the results.
    func detectLabelsCloud(withImage image: UIImage) {
       
        
        // [START init_label_cloud]
        let labelDetector = Vision.vision().cloudLabelDetector()
        // Or, to change the default settings:
        // let labelDetector = Vision.vision().cloudLabelDetector(options: options)
        // [END init_label_cloud]
        
        // Initialize a VisionImage object with a UIImage.
        let visionImage = VisionImage(image: image)
        
        // [START detect_label]
        labelDetector.detect(in: visionImage) { (labels, error) in
            
            
            
            
            guard error == nil, let tLabels = labels, !tLabels.isEmpty else {
                // Error. You should also check the console for error messages.
                // [START_EXCLUDE]
                let errorString = detectionNoResultsMessage
                print("Label detection failed with error: \(errorString)")
                self.resultsTextView.text = "No Objects!"
                // [END_EXCLUDE]
                
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.3, execute: {
                    if self.frameExtractor != nil {
                        self.frameExtractor.wait = false
                    }
                })
                
                return
            }
            
//            print("Labels :  \(tLabels)")
            // Labeled image
            // [START_EXCLUDE]
            
            
            self.resultsTextView.text = tLabels.map { label -> String in
                
                
                
                
                "\(String(describing: label.label))"
                }.joined(separator: "\n")
            
            
            
            for label in tLabels {
                if enemies.contains((label.label?.lowercased())!) {
                    self.enemiesDetected()
                 
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.3, execute: {
                if self.frameExtractor != nil {
                    self.frameExtractor.wait = false
                }
            })
            // [END_EXCLUDE]
        }
        // [END detect_label_cloud]
    }
    
    
    
    /// Returns the `VisionDetectorImageOrientation` from the given `UIImageOrientation`.
    private func detectorOrientationFrom(
        _ imageOrientation: UIImageOrientation
        ) -> VisionDetectorImageOrientation {
        switch imageOrientation {
        case .up:
            return .topLeft
        case .down:
            return .bottomRight
        case .left:
            return .leftBottom
        case .right:
            return .rightTop
        case .upMirrored:
            return .topRight
        case .downMirrored:
            return .bottomLeft
        case .leftMirrored:
            return .leftTop
        case .rightMirrored:
            return .rightBottom
        }
    }

    func enemiesDetected() {
        let systemSoundID: SystemSoundID = 1052
        AudioServicesPlaySystemSound (systemSoundID)
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        self.frameExtractor.delegate = nil
        self.frameExtractor = nil
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
            self.frameExtractor = FrameExtractor(previewView: self.videoV)
            self.frameExtractor.delegate = self
        }
    }
}

