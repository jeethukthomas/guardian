//
//  AddNewVC.swift
//  MLKit
//
//  Created by Jeethu Thomas on 24/05/18.
//  Copyright © 2018 Jeethu Thomas. All rights reserved.
//

import UIKit
import Firebase


class AddNewVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var itemTF: UITextField!
    let picker = UIImagePickerController()
     lazy var vision = Vision.vision()
    
    override func viewDidLoad() {
        super.viewDidLoad()
picker.delegate = self
        // Do any additional setup after loading the view.
    }

    @IBAction func clearObjectsFn(_ sender: Any) {
        
        let alert = UIAlertController(title: "Clear saved objects", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        let cancelBtn = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) in
            
        }
        let doneBtn = UIAlertAction(title: "Clear", style: UIAlertActionStyle.default) { (action) in
            UserDefaults.standard.set(["jeethu"], forKey: "Enemies")
        }
        alert.addAction(cancelBtn)
        alert.addAction(doneBtn)
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func backFn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func addObjFn(_ sender: UIButton) {
        if let str = self.itemTF.text {
            enemies.append(str.lowercased())
            self.itemTF.text = ""
            
            let alert = UIAlertController(title: "Added", message: "App will alert when ever it detects \(str) in the camera", preferredStyle: UIAlertControllerStyle.alert)
            let cancelBtn = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.destructive) { (action) in
                
            }
            alert.addAction(cancelBtn)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var  chosenImage = UIImage()
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2

        
        dismiss(animated: true) {
            self.detectLabels(withImage: chosenImage)
        }
    }
    
    
   
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated:true, completion: nil)
    }
    @IBAction func addItemFromImage(_ sender: Any) {
        self.view.endEditing(true)
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerControllerSourceType.camera
        picker.cameraCaptureMode = .photo
        picker.modalPresentationStyle = .fullScreen
        present(picker,animated: true,completion: nil)
    }
    
    
    func detectLabels(withImage image: UIImage) {
        
        // [START config_label]
        let options = VisionLabelDetectorOptions(
            confidenceThreshold: labelConfidenceThreshold
        )
        // [END config_label]
        
        // [START init_label]
        let labelDetector = vision.labelDetector(options: options)  // Check console for errors.
        // Or, to use the default settings:
        // let labelDetector = vision?.labelDetector()
        // [END init_label]
        
        // Define the metadata for the image.
        let imageMetadata = VisionImageMetadata()
        imageMetadata.orientation = detectorOrientationFrom(image.imageOrientation)
        
        // Initialize a VisionImage object with the given UIImage.
        let visionImage = VisionImage(image: image)
        visionImage.metadata = imageMetadata
        
        // [START detect_label]
        labelDetector.detect(in: visionImage) { (labels, error) in
            
            
            
            
            guard error == nil, let tLabels = labels, !tLabels.isEmpty else {
                // Error. You should also check the console for error messages.
                // [START_EXCLUDE]
                let errorString = detectionNoResultsMessage
                print("Label detection failed with error: \(errorString)")
//                self.resultsTextView.text = "No Objects!"
                // [END_EXCLUDE]
                
                
                
                
                return
            }
            
            print("Labels :  \(tLabels)")
            // Labeled image
            // [START_EXCLUDE]
            
            
//            self.resultsTextView.text = tLabels.map { label -> String in
//
//
//
//
//                "\(label.label)"
//                }.joined(separator: "\n")
            
            
            
            for label in tLabels {
                if !enemies.contains(label.label.lowercased()) {
                    enemies.append(label.label.lowercased())
                    
                }
            }
            // [END_EXCLUDE]
        }
        // [END detect_label]
    }
    
    /// Returns the `VisionDetectorImageOrientation` from the given `UIImageOrientation`.
    private func detectorOrientationFrom(
        _ imageOrientation: UIImageOrientation
        ) -> VisionDetectorImageOrientation {
        switch imageOrientation {
        case .up:
            return .topLeft
        case .down:
            return .bottomRight
        case .left:
            return .leftBottom
        case .right:
            return .rightTop
        case .upMirrored:
            return .topRight
        case .downMirrored:
            return .bottomLeft
        case .leftMirrored:
            return .leftTop
        case .rightMirrored:
            return .rightBottom
        }
    }

    
}
